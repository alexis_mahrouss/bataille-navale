#ifndef DESTROYER_H_INCLUDED
#define DESTROYER_H_INCLUDED

#include "boat.h"


class Destroyer:public Boat
{
public :
    Destroyer(int msize,int mtype,int morientation, int metat,bool mselect);
    virtual ~Destroyer();
    virtual void tirer();

};



#endif // DESTROYER_H_INCLUDED
