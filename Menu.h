#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include "ConsoleNavale.h"

class Menu
{
   public: //constructeur du menu
     Menu();
    ~Menu(); //Destructeur du menu

    void Menulancer();//Lancer le menu du jeu
    void afficher(ConsoleNavale* console);
    ///void charger();

    void Options(ConsoleNavale* console);
};

#endif // MENU_H_INCLUDED
