#include "case.h"

Case::Case() : m_x(-1),m_y(-1),m_etat(-1)
{}

Case::~Case()
{}

int Case::get_x()
{
    return this->m_x;
}
void Case::set_x(int x)
{
    this->m_x = x;
}
int Case::get_y()
{
    return this->m_y;
}
void Case::set_y(int y)
{
    this->m_y = y;
}
int Case::get_etat()
{
    return this->m_etat;
}
void Case::set_etat(int etat)
{
    this->m_etat = etat;
}
