#include "ConsoleNavale.h"
#include <conio.h>
#include <windows.h>

ConsoleNavale* ConsoleNavale::m_instance = NULL;

ConsoleNavale::ConsoleNavale() // Constructeur par d�faut
{
    m_instance = NULL;
}

ConsoleNavale::~ConsoleNavale() // Destructeur
{

}

ConsoleNavale* ConsoleNavale::getInstance() // Allouer de la m�moire pour le pointeur console
{
    if (!ConsoleNavale::m_instance)
    {
        m_instance = new ConsoleNavale();
    }

    return m_instance;
}

void ConsoleNavale::deleteInstance() // Supprimer la m�moire allou�e pour le pointeur console
{
    delete m_instance;
    m_instance = NULL;
}



void ConsoleNavale::gotoLigCol(int lig, int col) // D�placer le curseur dans la console
{
    COORD mycoord;
    mycoord.X = col;
    mycoord.Y = lig;
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), mycoord );
}

bool ConsoleNavale::isKeyboardPressed() //Savoir si l'utilisateur a appuy� sur une touche
{
    return kbhit();
}

int ConsoleNavale::getInputKey() // R�cup�rer la touche appuy�e par l'utilisateur
{
    return getch();
}


void ConsoleNavale::clean() // Supprimer tout ce qui �tait �crit sur la console
{
    system("cls");
}

void ConsoleNavale::pause() // Mettre le programme en pause : l'utilisateur doit appuyer sur une touche pour reprendre l'ex�cution du programme
{
    system("pause");
}


