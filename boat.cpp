#include "boat.h"

Boat::Boat(int msize,int mtype,int morientation, int metat,bool mselect)
    :m_size(msize),m_type(mtype),m_orientation(morientation),m_etat(metat), m_select( mselect), m_case()
{

}

Boat::~Boat()
{

}

int Boat::get_size()
{
    return this->m_size;
}
int Boat::get_type()
{
    return this->m_type;
}
int Boat::get_orientation()
{
    return this->m_orientation;
}
int Boat::get_etat()
{
    return this->m_etat;
}
bool Boat::get_select()
{
    return this->m_select;
}
std::vector<Case> Boat::get_case()
{
    return this->m_case;
}

void Boat::set_size(int size)
{
    this->m_size=size;
}
void Boat::set_type(int type)
{
    this->m_type=type;
}
void Boat::set_orientation(int orientation)
{
    this->m_orientation=orientation;
}
void Boat::set_etat(int etat)
{
    this->m_etat=etat;
}
void Boat::set_select(bool select)
{
    this->m_select=select;
}
void Boat::set_case(std::vector<Case> cases)
{
    m_case = cases;

}

bool Boat::turn_boat(std::vector<Boat*> boats)
{
    int nb_case=get_size()/2;
    int case_turn = 0;
    int y,x;
    bool block = false;

    if(get_orientation()==ORIENTATION_N_S)
    {
        y = get_case()[nb_case].get_y();
        x = get_case()[nb_case].get_x();

        for(int i=0; i<nb_case; i++)
        {
            for(int j=0; j<boats.size(); j++)
            {
                for(int k=0; k<boats[i]->get_size(); k++)
                {
                    if (((boats[i]->get_case()[k].get_x()==x-i)&&(boats[i]->get_case()[k].get_y()==y))||((boats[i]->get_case()[k].get_x()==x+i)&&(boats[i]->get_case()[k].get_y()==y)))
                    {
                        block=true;
                    }
                }
            }
        }

        if(!block)
        {

            for(int i=nb_case; i>0; i--)
            {
                get_case()[case_turn].set_x(get_case()[case_turn].get_x()-i);
                get_case()[case_turn].set_y(get_case()[nb_case].get_y());
                case_turn++;
            }
            case_turn=case_turn+2;
            for(int i=1; i<=nb_case; i++)
            {
                get_case()[case_turn].set_x(get_case()[case_turn].get_x()+i);
                get_case()[case_turn].set_y(get_case()[nb_case].get_y());
                case_turn++;
            }

        }
    }
    if(get_orientation()==ORIENTATION_E_O)
    {
        y = get_case()[nb_case].get_y();
        x = get_case()[nb_case].get_x();

        for(int i=0; i<nb_case; i++)
        {
            for(int j=0; j<boats.size(); j++)
            {
                for(int k=0; k<boats[i]->get_size(); k++)
                {
                    if (((boats[i]->get_case()[k].get_y()==y-i)&&(boats[i]->get_case()[k].get_x()==x))||((boats[i]->get_case()[k].get_y()==y+i)&&(boats[i]->get_case()[k].get_x()==x)))
                    {
                        block=true;
                    }
                }
            }
        }

        if(!block)
        {
            for(int i=nb_case; i>0; i--)
            {
                get_case()[case_turn].set_y(get_case()[case_turn].get_y()-i);
                get_case()[case_turn].set_x(get_case()[nb_case].get_x());
                case_turn++;
            }
            case_turn=case_turn+2;
            for(int i=1; i<=nb_case; i++)
            {
                get_case()[case_turn].set_y(get_case()[case_turn].get_y()+i);
                get_case()[case_turn].set_x(get_case()[nb_case].get_x());
                case_turn++;
            }
        }
    }
    return block;
}



bool Boat::test_case(int x, int y, std::vector<Boat*> boats)
{
    bool test=false;
    for(int i=0; i<boats.size(); i++)
    {
        for(int j=0; j<boats[i]->get_size(); j++)
        {
            if((boats[i]->get_case()[j].get_x()==x)&&(boats[i]->get_case()[j].get_y()==y))
            {
                test=true;
            }
        }
    }
    if(x<0)
    {
        test=true;
    }
    if(y<0)
    {
        test=true;
    }
    if(x>=SIZE_WIDTH)
    {
        test=true;
    }
    if(y>=SIZE_HEIGHT)
    {
        test=true;
    }
    return test;
}


bool Boat::move_boat(std::vector<Boat*> boats)
{
    bool block = true;
    bool stop = false;
    char key;

    ///afficher les instruction pour jouer

    while(!stop)
    {
        while(block)
        {
            key= getchar();

            switch(key)
            {
            case 'z':
                if(get_orientation()==ORIENTATION_N_S)
                {
                    if(get_size()==1)
                    {
                        block=test_case(get_case()[0].get_x(),get_case()[0].get_y()-1,boats);
                        if(!block)
                        {
                            get_case()[0].set_y(get_case()[0].get_y()-1);
                            stop=true;

                        }
                    }
                    else
                    {
                        if(get_case()[0].get_y()<get_case()[1].get_y())
                        {
                            block=test_case(get_case()[0].get_x(),get_case()[get_size()-1].get_y()-1,boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    get_case()[i].set_y(get_case()[i].get_y()-1);
                                    stop=true;
                                }
                            }
                        }
                        else
                        {
                            block=test_case(get_case()[0].get_x(),get_case()[0].get_y()-1,boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    stop=true;
                                    get_case()[i].set_y(get_case()[i].get_y()-1);
                                }
                            }
                        }
                    }

                }
                else
                {
                    block=true;
                }
                break;

            case 's':

                if(get_orientation()==ORIENTATION_N_S)
                {
                    if(get_size()==1)
                    {
                        block=test_case(get_case()[0].get_x(),get_case()[0].get_y()+1,boats);
                        if(!block)
                        {
                            get_case()[0].set_y(get_case()[0].get_y()+1);
                            stop=true;
                        }
                    }
                    else
                    {
                        if(get_case()[0].get_y()<get_case()[1].get_y())
                        {
                            block=test_case(get_case()[0].get_x(),get_case()[0].get_y()+1,boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    get_case()[i].set_y(get_case()[i].get_y()+1);
                                    stop=true;
                                }
                            }
                        }
                        else
                        {
                            block=test_case(get_case()[0].get_x(),get_case()[get_size()-1].get_y()+1,boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    get_case()[i].set_y(get_case()[i].get_y()+1);
                                    stop=true;
                                }

                            }
                        }
                    }

                }
                else
                {
                    block=true;
                }
                break;

            case 'q':

                if(get_orientation()==ORIENTATION_E_O)
                {
                    if(get_size()==1)
                    {
                        block=test_case(get_case()[0].get_x()-1,get_case()[0].get_y(),boats);
                        if(!block)
                        {
                            get_case()[0].set_x(get_case()[0].get_x()-1);
                            stop=true;
                        }
                    }
                    else
                    {
                        if(get_case()[0].get_x()<get_case()[1].get_x())
                        {
                            block=test_case(get_case()[0].get_x()-1,get_case()[0].get_y(),boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {

                                    stop=true;
                                    get_case()[i].set_x(get_case()[i].get_x()-1);
                                }
                            }
                        }
                        else
                        {
                            block=test_case(get_case()[get_size()-1].get_x()-1,get_case()[0].get_y(),boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    get_case()[i].set_x(get_case()[i].get_x()-1);

                                    stop=true;
                                }
                            }
                        }
                    }

                }
                else
                {
                    block=true;
                }
                break;

            case 'd':

                if(get_orientation()==ORIENTATION_E_O)
                {
                    if(get_size()==1)
                    {
                        block=test_case(get_case()[0].get_x()-1,get_case()[0].get_y(),boats);
                        if(!block)
                        {
                            get_case()[0].set_x(get_case()[0].get_x()+1);

                            stop=true;
                        }
                    }
                    else
                    {
                        if(get_case()[0].get_x()<get_case()[1].get_x())
                        {
                            block=test_case(get_case()[get_size()-1].get_x()+1,get_case()[0].get_y(),boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    get_case()[i].set_x(get_case()[i].get_x()+1);
                                    stop=true;
                                }
                            }
                        }
                        else
                        {
                            block=test_case(get_case()[0].get_x()+1,get_case()[0].get_y(),boats);
                            if(!block)
                            {
                                for(int i=0; i<get_size(); i++)
                                {
                                    get_case()[i].set_x(get_case()[i].get_x()+1);
                                    stop=true;
                                }
                            }
                        }
                    }

                }
                else
                {
                    block=true;
                }

                break;
            default:
                stop=true;
                break;
            }

        }
    }
    return block;

}
