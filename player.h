#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include "map.h"

class Player
{
private:
    int m_number;

    Map m_map;
    Field m_field;

public:
    Player();
    ~Player();

    void set_number(int number);
    int get_number();
    Map get_map();
    Field get_field();

    void set_map(Map mape);
    void set_field(Field field);

    void creat_player(int num_player);
    void jouer(Player ennemy_player);


};



#endif // PLAYER_H_INCLUDED
