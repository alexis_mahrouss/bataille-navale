#include "map.h"

Map::Map():m_size_x(SIZE_WIDTH),m_size_y(SIZE_HEIGHT),m_affichage()
{}

Map::~Map()
{}

int Map::get_size_x()
{
    return this->m_size_x;
}
void Map::set_size_x(int x)
{
    this->m_size_x=x;
}
int Map::get_size_y()
{
    return this->m_size_y;
}
void Map::set_size_y(int y)
{
    this->m_size_y=y;
}

std::vector<std::vector<char>> Map::get_affichage()
{
    return this->m_affichage;
}
void Map::set_affichage(std::vector<std::vector<char>> affichage)
{
    m_affichage=affichage;
}


void Map::afficher_carte(Field ennemy_f)
{
    int coor_x, coor_y;
    for(int j=0; j<get_size_y(); j++)
    {
        for(int i=0; i<get_size_x(); i++)
        {
            m_affichage[i][j]=0;
        }
    }
    for(int k=0; k<ennemy_f.get_boat().size(); k++)
    {
        for(int l=0; l<ennemy_f.get_boat()[k]->get_size(); l++)
        {
            coor_x=ennemy_f.get_boat()[k]->get_case()[l].get_x();
            coor_y=ennemy_f.get_boat()[k]->get_case()[l].get_y();
            if(ennemy_f.get_boat()[k]->get_case()[l].get_etat()==ETAT_C_TOUCHER)
            {
                m_affichage[coor_x][coor_y]=ETAT_C_TOUCHER;
            }
        }
    }
    for(int j=0; j<get_size_y(); j++)
    {
        for(int i=0; i<get_size_x(); i++)
        {
            std::cout <<m_affichage[i][j]<<"|";
        }
        std::cout<<" "<<std::endl;
    }
}
