#ifndef SOUS_MARIN_H_INCLUDED
#define SOUS_MARIN_H_INCLUDED


#include "boat.h"


class Sous_Marin:public Boat
{
public :
    Sous_Marin(int msize,int mtype,int morientation, int metat,bool mselect);
    virtual ~Sous_Marin();
    virtual void tirer();

};



#endif // SOUS_MARIN_H_INCLUDED
