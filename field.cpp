#include "field.h"

Field::Field() : m_size_x(SIZE_WIDTH),m_size_y(SIZE_HEIGHT),m_boat(),m_affichage()
{}

Field::~Field()
{}

int Field::get_size_x()
{
    return this->m_size_x;
}
void Field::set_size_x(int x)
{
    this->m_size_x = x;
}
int Field::get_size_y()
{
    return this->m_size_y;
}
void Field::set_size_y(int y)
{
    this->m_size_y = y;
}

std::vector<Boat*> Field::get_boat()
{
    return this->m_boat;
}

void Field::set_boat()
{
    srand(time(NULL));
    int remplissage[get_size_x()][get_size_y()];
    int por,px, py;
    for(int j=0; j<get_size_y(); j++)
    {
        for(int i=0; i<get_size_x(); i++)
        {
            remplissage[i][j]=0;
        }
    }
    for(int i=0; i<m_boat.size(); i++)
    {
        bool test = true;
        do
        {
            test = true;
            por = rand()%2;
            px = rand()% get_size_x() ;
            py = rand()% get_size_y() ;
            m_boat[i]->set_orientation(por);
            if (por == ORIENTATION_N_S)
            {
                if (m_boat[i]->get_size()+ py < get_size_y())
                {
                    for(int k = py; k <= m_boat[i]->get_size()+ py -1 ; k++ )
                    {
                        if (remplissage[px][k] == 1)
                        {
                            test = false;
                        }
                    }
                }
                else test = false;
            }

            if (por == ORIENTATION_E_O)
            {
                if (m_boat[i]->get_size()+ px < get_size_x())
                {
                    for(int k = px; k <= m_boat[i]->get_size()+ px -1 ; k++ )
                    {
                        if (remplissage[k][py] == 1)
                        {
                            test = false;
                        }
                    }

                }
                else test = false;
            }

        }
        while (test == false);
        std::vector<Case> cases;
        if (por == ORIENTATION_N_S)
        {
            for(int k = py; k <= m_boat[i]->get_size()+ py -1 ; k++ )
            {
                Case nc ;
                nc.set_x(px);
                nc.set_y(k);
                nc.set_etat(0);
                cases.push_back(nc);
                remplissage[px][k] = 1;
            }
        }
        if (por == ORIENTATION_E_O)
        {
            for(int k = px; k <= m_boat[i]->get_size()+ px -1 ; k++ )
            {
                Case nc ;
                nc.set_x(k);
                nc.set_y(py);
                nc.set_etat(0);
                cases.push_back(nc);
                remplissage[k][py] = 1;
            }

        }
        m_boat[i]->set_case(cases);
    }

    std::cout<<m_boat.size();
}











std::vector<std::vector<char>> Field::get_affichage()
{
    return this->m_affichage;
}
void Field::set_affichage(std::vector<std::vector<char>> affichage)
{
    m_affichage=affichage;
}

void Field::creation_terain()
{
    std::vector<Boat*> boats;
    m_boat=boats;
    printf("coucou");
    for(int i=0; i<NB_CROISEUR; i++)
    {
        m_boat.push_back(new Croiseur(5,TYPE_CROISEUR,-1,ETAT_B_NORMAL,true));
    }
    for(int i=0; i<NB_CUIRASSE; i++)
    {
        m_boat.push_back(new Cuirasse(7,TYPE_CUYRASSE,-1,ETAT_B_NORMAL,true));
    }
    for(int i=0; i<NB_DESTROYER; i++)
    {
        m_boat.push_back(new Destroyer(3,TYPE_DESTROYER,-1,ETAT_B_NORMAL,true));
    }
    for(int i=0; i<NB_SOUS_MARIN; i++)
    {
        m_boat.push_back(new Sous_Marin(1,TYPE_SOUS_MARIN,-1,ETAT_B_NORMAL,true));
    }

    std::cout<<"il y en a "<<m_boat.size()<<std::endl;
}

void Field::afficher_terrain()
{
    Boat *bateau;

    int coor_x,coor_y;

    for(int j=0; j<get_size_y(); j++)
    {
        for(int i=0; i<get_size_x(); i++)
        {
            m_affichage[i][j]=0;
        }
    }
    for(int k=0; k<get_boat().size(); k++)
    {
        for(int l=0; l<get_boat()[k]->get_size(); l++)
        {
            coor_x=get_boat()[k]->get_case()[l].get_x();
            coor_y=get_boat()[k]->get_case()[l].get_y();
            if(get_boat()[k]->get_select()==false)
            {
                m_affichage[coor_x][coor_y]=get_boat()[k]->get_case()[l].get_etat();
            }
            else
            {
                m_affichage[coor_x][coor_y]=ETAT_SELECT;
            }

        }
    }
    for(int j=0; j<get_size_y(); j++)
    {
        for(int i=0; i<get_size_x(); i++)
        {
            std::cout <<m_affichage[i][j]<<"|";
        }
        std::cout<<" "<<std::endl;
    }

}


