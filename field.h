#ifndef FIELD_H_INCLUDED
#define FIELD_H_INCLUDED

#include"cuirasse.h"
#include"croiseur.h"
#include"destroyer.h"
#include"sous_marin.h"

#define NB_CUIRASSE 1
#define NB_CROISEUR 2
#define NB_DESTROYER 3
#define NB_SOUS_MARIN 4
#define NB_BOAT 10
#define ETAT_SELECT 10


class Field
{

private:
    ///tailles de la carte
    int m_size_x;
    int m_size_y;
    std::vector<std::vector<char>> m_affichage;


public:
    ///vecteurs avec les bateaux
    std::vector<Boat*> m_boat;

    ///creator, destructor
    Field();
    ~Field();

    ///getter setter
    int get_size_x();
    void set_size_x(int x);
    int get_size_y();
    void set_size_y(int y);
    std::vector<Boat*> get_boat();
    void set_boat();
    std::vector<std::vector<char>> get_affichage();
    void set_affichage(std::vector<std::vector<char>> affichage);

    ///fonction
    void creation_terain();
    void afficher_terrain();
};




#endif // FIELD_H_INCLUDED
