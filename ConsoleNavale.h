#ifndef CONSOLENAVALE_H_INCLUDED
#define CONSOLENAVALE_H_INCLUDED



#include <iostream>



class ConsoleNavale
{
    private:
        // Empecher la cr�ation
        ConsoleNavale();
        ~ConsoleNavale();

        // Empecher la copie d'objet...
        ConsoleNavale& operator= (const ConsoleNavale&){ return *this;}
        ConsoleNavale (const ConsoleNavale&){}

        // Attributs
        static ConsoleNavale* m_instance;

        // M�thodes priv�es

    public:
        // M�thodes statiques (publiques)
        static ConsoleNavale* getInstance();// Allouer de la m�moire pour le pointeur console
        static void deleteInstance();// Supprimer la m�moire allou�e pour le pointeur console

        // M�thodes publiques
        void gotoLigCol(int lig, int col);// D�placer le curseur dans la console
        bool isKeyboardPressed();//Savoir si l'utilisateur a appuy� sur une touche
        int getInputKey();// R�cup�rer la touche appuy�e par l'utilisateur
        void clean();// Supprimer tout ce qui �tait �crit sur la console
        void pause();// Mettre le programme en pause : l'utilisateur doit appuyer sur une touche pour reprendre l'ex�cution du programme

};




#endif // CONSOLENAVALE_H_INCLUDED
