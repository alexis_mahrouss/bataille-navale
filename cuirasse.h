#ifndef CUIRASSE_H_INCLUDED
#define CUIRASSE_H_INCLUDED

#include "boat.h"


class Cuirasse:public Boat
{
public :
    Cuirasse(int msize,int mtype,int morientation, int metat,bool mselect);
    virtual ~Cuirasse();
    virtual void tirer();

};




#endif // CUIRASSE_H_INCLUDED
