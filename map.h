#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED


#include "field.h"

#define SIZE_WIDTH 15
#define SIZE_HEIGHT 15


class Map
{

private:
    ///tailles de la carte
    int m_size_x;
    int m_size_y;
    ///vecteurs avec les bateaux


    std::vector<std::vector<char>> m_affichage;


public:
    ///creator, destructor
    Map();
    ~Map();

    ///getter setter
    int get_size_x();
    void set_size_x(int x);
    int get_size_y();
    void set_size_y(int y);
    std::vector<Case*> get_case();
    void set_case(std::vector<Case*> cases);
    std::vector<std::vector<char>> get_affichage();
    void set_affichage(std::vector<std::vector<char>> affichage);
    void afficher_carte(Field ennemy_f);
};



#endif // MAP_H_INCLUDED
