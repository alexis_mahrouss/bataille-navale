#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "player.h"


class Game
{

private:
    Player m_player1;
    Player m_player2;

public:
    Game();
    ~Game();

    Player get_player1();
    Player get_player2();

    void set_player1( Player p1);
    void set_player2( Player p2);

    void debut();


};

#endif // GAME_H_INCLUDED
