#ifndef BOAT_H_INCLUDED
#define BOAT_H_INCLUDED

#include "case.h"

#define SIZE_WIDTH 15
#define SIZE_HEIGHT 15

enum
{
    TYPE_CUYRASSE = 1, TYPE_CROISEUR = 2,TYPE_DESTROYER=3,TYPE_SOUS_MARIN=4
};

enum
{
    ETAT_B_NORMAL= 1, ETAT_B_TOUCHER=2, ETAT_B_COULER= 3
};

enum
{
    ORIENTATION_N_S=0, ORIENTATION_E_O=1
};

class Boat
{

protected:
    ///tailles de la carte
    int m_size;
    int m_type;
    int m_orientation;
    int m_etat;
    bool m_select;

    ///vecteurs avec les bateaux
    std::vector<Case> m_case;

public:
    ///creator, destructor
    Boat(int msize,int mtype,int morientation, int metat,bool mselect);
    virtual ~Boat();

    ///getter setter
    int get_size();
    int get_type();
    int get_orientation();
    int get_etat();
    bool get_select();
    std::vector<Case> get_case();

    void set_size(int size);
    void set_type(int type);
    void set_orientation(int orientation);
    void set_etat(int etat);
    void set_select(bool select);
    void set_case(std::vector<Case> cases);
    virtual void tirer() = 0;
    bool turn_boat(std::vector<Boat*> boats);
    bool move_boat(std::vector<Boat*> boats);
    bool test_case(int x, int y,std::vector<Boat*> boats);

};

#endif // BOAT_H_INCLUDED
