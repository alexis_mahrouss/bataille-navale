#ifndef CASE_H_INCLUDED
#define CASE_H_INCLUDED

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "time.h"

enum
{
    ETAT_C_NORMAL =1, ETAT_C_TOUCHER=2
};


class Case
{

private:
    ///coordon�s
    int m_x;
    int m_y;
    ///variable d'�tat
    int m_etat;

public:
    ///creator, destructor
    Case();
    ~Case();

    ///getter setter
    int get_x();
    void set_x(int x);
    int get_y();
    void set_y(int y);
    int get_etat();
    void set_etat(int etat);

};


#endif // CASE_H_INCLUDED
