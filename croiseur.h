#ifndef CROISEUR_H_INCLUDED
#define CROISEUR_H_INCLUDED

#include "boat.h"

class Croiseur:public Boat
{
public :
    Croiseur(int msize,int mtype,int morientation, int metat,bool mselect);
    virtual ~Croiseur();
    virtual void tirer();

};


#endif // CROISEUR_H_INCLUDED
